<?
$post = new \TSD_Infinisite\IS_Post(get_the_ID());
$img = TSD_Infinisite\Acme::get_image_info_by_id($post->get("image")['id']);

$module_config = ['acf_fc_layout'  => 'hero',
                  'title'          => "",
                  'image'          => $img,
                  'content_blocks' => [['wysiwyg' => "<h3>Our Services</h3>
<p>Getting business done, no matter what it takes.</p>"]],];


$hero_module = new \TSD_Infinisite\ACF_Module(['module'          => $module_config,
                                               'module_template' => $_SERVER['DOCUMENT_ROOT'] . '/wp-content/themes/is_child/module_templates/hero/apc_home.php',]);


?>

<div class="apc-single-content" data-editor-style>
    <div class="grid-x grid-padding-x">
        <div class="cell no-padding-right">

            <?= $hero_module->get_html(); ?>

            <div class="spacer"></div>

            <div class="grid-x grid-padding-x align-stretch">

                <div class="cell small-12 medium-3 sidebar">
                    <div class="show-for-medium">
                        <h3>&nbsp;</h3>
                    </div>
                    <div class="clear" data-js-stick-to-window data-js-sticky-offset="15">
                        <div class="meta_box secondary-background">

                            <h6>View our Projects</h6>
                            <ul class="menu vertical">
                                <? foreach (get_projects() as $service): ?>
                                    <li>
                                        <? $active = $service->ID == $post->ID ? 'active' : '' ?>
                                        <a href="<?= $service->permalink ?>" class="<?= $active ?>">
                                            <?= $service->post_title ?>
                                        </a>
                                    </li>
                                <? endforeach ?>
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="cell small-12 medium-auto content no-padding-right">
                    <h4><?= $post->post_title ?></h4>
                    <p>Content here, lorem ipsum dolor sit amet.</p>

                    <?= \TSD_Infinisite\Acme::get_file("components/single-view/cta.php", ['id' => 12]) ?>

                </div>
            </div>
        </div>
    </div>
</div>
