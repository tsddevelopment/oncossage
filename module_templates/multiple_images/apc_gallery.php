<? $gal_id = uniqid('gallery_') ?>
<div class="apc-gallery">
    <? foreach ($module->get("images") as $image): ?>
        <a class="image"
           href="<?= $image['sizes']['large'] ?>"
           data-fancybox="<?= $gal_id ?>"
           data-caption="<?= $image['caption'] ?>"
           style="background-image: url(<?= $image['sizes']['large'] ?>)">
        </a>
    <? endforeach ?>
</div>
