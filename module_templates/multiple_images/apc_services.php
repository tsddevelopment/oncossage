<? $gal_id = uniqid('gallery_') ?>
<div class="cell apc-services-gallery">
    <div class="grid-x">
        <div class="cell small-12 medium-shrink text-center flex-column align-center-middle">
            <h3>Our Services</h3>

            <p class="no-margin">
                <a href="#" class="button">Request a Quote</a>
            </p>

        </div>
        <div class="cell small-12 medium-auto">
            <div class="grid-x small-up-1 medium-up-3 align-middle grid-padding-x">
                <? foreach ($module->get("images") as $image): ?>
                    <div class="cell">

                        <a class="image"
                           href="<?= $image['sizes']['large'] ?>"
                           data-fancybox="<?= $gal_id ?>"
                           data-caption="<?= $image['caption'] ?>"
                           style="background-image: url(<?= $image['sizes']['large'] ?>)">
                            <img src="<?= $image['sizes']['medium'] ?>" alt="<?= $image['alt_text'] ?>">
                        </a>
                    </div>
                <? endforeach ?>
            </div>

        </div>
    </div>
</div>

