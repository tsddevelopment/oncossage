<?
$title = get_the_title();

if ( array_key_exists( 'title', $module ) )
    $title = $module['title'][0]->title;

?>

<div class="onc-interior-hero">
    <h2 class="secondary-text no-margin"><?= $title ?></h2>
</div>
<div class="spacer"></div>

<?= onc_coming_soon_banner() ?>