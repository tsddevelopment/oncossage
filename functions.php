<?

add_action( "wp", 'onc_home_page_splash' );

function onc_home_page_splash() {


    $home_page      = is_front_page();
    $user_logged_in = is_user_logged_in();
    $has_cookie     = isset( $_COOKIE['onc_visit_time'] );
    $show_splash    = $home_page && ! $has_cookie;

    if ( ! $show_splash )
        return;

    $visit_time = date( 'F j, Y g:i a' );
    setcookie( 'onc_visit_time', $visit_time, time() + 1209600 ); // 2 week expiry

    ob_start();
    include __DIR__ . '/components/popup/video-first-visit.php';
    $modal_content = ob_get_clean();

    print $modal_content;

}

add_shortcode( "onc_coming_soon_banner", function() {
    return onc_coming_soon_banner();
} );


function onc_coming_soon_banner() {
    return \TSD_Infinisite\Acme::get_file( "components/coming_soon_banner.php" );
}


function onc_get_login_link() {
    $login_link = '
        <healcode-widget data-version="0.2" data-link-class="loginRegister" data-site-id="85035" data-mb-site-id="415495" data-type="account-link" data-inner-html="Login | Register"  />
        ';

    return $login_link;
}

//function onc_first_time_visit_cookie() {
// Time of user's visit
$visit_time = date( 'F j, Y g:i a' );

//    echo "<h3>{$_COOKIE['onc_visit_time']}</h3><textarea>".print_r($_COOKIE, true)."</textarea>";
// Check if cookie is already set
//    if (isset($_COOKIE['onc_visit_time']) && false) {
//
//         Do this if cookie is set
//        function visitor_greeting() {
//            return "";
//        }
//
//    } else {

// Do this if the cookie doesn't exist
function visitor_greeting() {
    ob_start();
    include __DIR__ . '/components/popup/video-first-visit.php';
    $modal_content = ob_get_clean();
    $string        = '
                <div id="videoModalFirstVisit">
                  ' . $modal_content . '
                  <a class="close-reveal-modal" aria-label="Close" id="videoModalFirstVisitCloseBtn">&#215;</a>
                </div>';

    return $string;


    // Set the cookie with 2 week expiry
    setcookie( 'onc_visit_time', $visit_time, time() + 1209600 );

}


//         Add a shortcode
add_shortcode( 'first_visit_popup', 'visitor_greeting' );

//    }
//}


//onc_first_time_visit_cookie();

add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_script( 'jquery-matchheight', 'https://cdnjs.cloudflare.com/ajax/libs/jquery.matchHeight/0.7.2/jquery.matchHeight-min.js', [ 'jquery' ], null, true );
    wp_enqueue_script( 'mbo_widget', 'https://widgets.healcode.com/javascripts/healcode.js' );
} );

TSD_Infinisite\Shortcode::create( 'home_movie' );
TSD_Infinisite\Shortcode::create( 'mbo_frame' );
TSD_Infinisite\Shortcode::create( 'application_email_form' );


//Your "container" content element should extend WPBakeryShortCodesContainer class to inherit all required functionality
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Your_Gallery extends WPBakeryShortCodesContainer {
    }
}
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Single_Img extends WPBakeryShortCode {
    }
}

add_image_size( "Home Circle", 600, 600, true );
add_image_size( "Home Slideshow", 1300, 300, true );

add_filter( 'protected_title_format', 'remove_protected_text' );
function remove_protected_text() {
    return __( '%s' );
}

//
//add_action( 'wp', function() {
//
//    print 'check';
//    if ( array_key_exists( "email", $_GET ) ) {
//
//        print get_the_ID();
//
//    }
//} );
