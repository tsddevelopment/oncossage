<div class="cell small-12 apc-testimonial-slider-container is_post_archive_query_module">

    <div class="grid-x grid-padding-x">
        <div class="cell small-12">
            <h4 class="">Read what people are saying</h4>
            <div class="spacer small"></div>

            <? foreach ($this->query->posts as $c => $post): ?>
                <? $post = new \TSD_Infinisite\IS_Post($post->ID) ?>
                <div class="grid-x grid-padding-x align-middle testimonial">

                    <div class="cell auto">
                        <?= $post->get("content") ?>
                        <h6 class="text-right"> &mdash; <?= $post->post_title ?></h6>

                    <? if($c + 1 != count($this->query->posts)): ?>

                    <?= divider('left') ?>

                    <? endif ?>
                    </div>

                </div>

            <? endforeach ?>
        </div>
    </div>


</div>