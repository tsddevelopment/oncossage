<? $post = \TSD_Infinisite\IS_Post::db_get(13) ?>

<div class="cell small-12 apc-testimonial-slider-container is_post_archive_query_module">

    <div class="grid-x grid-padding-x">
        <div class="cell small-12 medium-3 flex-column align-center">
            <h3 class="text-right">
                <a href="<?= $post->permalink ?>" class="secondary-text">
                    Read what<br />people are<br />saying
                </a>
            </h3>
        </div>
        <div class="cell small-12 medium-auto">
            <div class="owl-carousel apc-testimonial-slider">

                <? foreach ($this->query->posts as $post): ?>
                    <? $post = new \TSD_Infinisite\IS_Post($post->ID) ?>
                    <div class="grid-x grid-padding-x align-middle">

                        <div class="cell auto">
                            <?= $post->get("content") ?>
                            <h6> &mdash; <?= $post->post_title ?></h6>
                        </div>
                    </div>

                <? endforeach ?>
            </div>
        </div>
    </div>


</div>