<? $service_parent_post = \TSD_Infinisite\IS_Post::db_get(8);
$mid = uniqid('modal_');
$count = count($this->query->posts);
if($count >= 4) $count = 4;

?>
<div class="cell small-12 apc-services-gallery is_post_archive_query_module">

    <div class="grid-x grid-padding-x">


        <div class="cell small-12 medium-shrink flex-column align-center">
            <h3 class="text-center">
                <a href="<?= $service_parent_post->permalink ?>">
                    Our Services
                </a>
            </h3>
            <div class="spacer small"></div>
            <p class="text-center no-margin">
                <a href="<?= $service_parent_post->permalink ?>" class="button no-margin hollow">
                    Request a Quote
                </a>
            </p>
        </div>

        <div class="cell small-12 medium-auto space-left">
            <div class="grid-x small-up-2 medium-up-<?= $count ?> service-container align-middle">

                <? foreach ($this->query->posts as $post)
                    print tile($post); ?>
            </div>
        </div>
    </div>
</div>