<?php
$calendar = new \TSD_Infinisite\Calendar($this->query->posts);

$map = new \TSD_Infinisite\Google_Map(['height' => '300px']);

foreach ($this->query->posts as $post):
    $fields = new \TSD_Infinisite\ACF_Helper($post);
    $map->add_marker($fields->get("location"));
endforeach;


?>

<div class="cell">

    <div class="grid-x grid-padding-x">
        <div class="cell small-12 medium-4">
            <?= $map->get_html(); ?>
            <div class="spacer"></div>
            <?php foreach ($this->query->posts as $post): ?>
                <? $post = new \TSD_Infinisite\IS_Post($post->ID) ?>
                <div class="card">
                    <div class="card-section">
                        <h5><?= $post->post_title ?></h5>
                        <p><?= $post->get_start_date()->format("l, F jS") ?></p>
                        <p><a href="<?= $post->permalink ?>">Learn More</a></p>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="cell small-12 medium-4 large-auto">

            <?= $calendar->get_output('month') ?>
        </div>
    </div>


</div>