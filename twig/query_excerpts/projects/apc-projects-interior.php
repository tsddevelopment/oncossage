<div class="cell small-12 is_post_archive_query_module">

    <div class="grid-x">

        <div class="cell small-12 medium-auto">
            <div class="grid-x grid-padding-x grid-padding-y small-up-2">

                <? foreach ($this->query->posts as $post)
                    print tile($post); ?>
            </div>
        </div>
    </div>
</div>
