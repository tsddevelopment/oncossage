<? $landing_page_post = \TSD_Infinisite\IS_Post::db_get(12) ?>

<div class="cell small-12 apc-home-projects is_post_archive_query_module">

    <div class="grid-x">

        <div class="cell small-12 medium-auto">
            <div class="grid-x grid-padding-x small-up-3">

                <? foreach ($this->query->posts as $post)
                    print tile($post); ?>
            </div>
        </div>
        <div class="cell small-12 medium-3 flex-column align-center">
            <h4 class="primary-text">
                <a href="<?= $landing_page_post->permalink ?>">
                    Check out our projects
                </a>
            </h4>
            <p><a href="<?= $landing_page_post->permalink ?>">
                    <i class="fal fa-long-arrow-right fa-3x secondary-text"></i> </a>
            </p>
        </div>
    </div>
</div>
