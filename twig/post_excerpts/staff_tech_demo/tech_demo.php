<div class="cell">
    <div class="card primary">


        <? if ($post->get("image")): ?>
            <a href="<?= $post->permalink ?>">
                <img src="<?= $post->get("image")['url'] ?>" alt="<?= $post->post_title ?>" class="full-width">
            </a>
        <? endif ?>

        <div class="card-divider text-center">
            <a href="<?= $post->permalink ?>" class="full-width">
                <h5 class="no-margin">
                    <?= $post->post_title ?>
                </h5>
            </a>

            <? if ($post->get("start_date")): ?>
                <p class="gray_dark-text auto-text no-bottom-margin auto-left">
                    <small>
                        <?= $post->get_start_date()->format('F jS, Y') ?>
                    </small>
                </p>
            <? endif ?>

        </div>


        <div class="card-section">


            <div class="grid-container grid-x grid-padding-x no-padding no-margin">

                <div class="cell small-12">
                    <?= $post->get("excerpt") ?>
                </div>
                <div class="cell auto">
                    <? if ($post->permalink): ?>
                        <p><a href="<?= $post->permalink ?>">Read More</a></p>
                    <? endif ?>
                </div>
                <div class="cell auto">
                    <? if ($post->get("email_address")): ?>

                        <p><a href="mailto:<?= $post->get("email_address") ?>">Email Me</a></p>

                    <? endif ?>
                </div>

            </div>


        </div>
    </div>
</div>
