<div class="cell">
    <div class="card primary">

        <div class="card-divider">
            <a href="<?= $post->permalink ?>">
                <h5 class="no-margin">
                    <?= $post->post_title ?>
                </h5>
            </a>
        </div>


        <div class="card-section">


            <div class="grid-container grid-x grid-paddings-x no-padding no-margin">


                <div class="cell auto">
                    <? $date = $post->wp_obj->post_date ?>
                    <?= $post->get("excerpt") ?>
                    <h6>
                        <em>
                            <?= 'Posted on ' . date('l, F j, Y', strtotime($date)) ?>
                        </em>
                    </h6>
                    <? if ($post->permalink): ?>
                        <a href="<?= $post->permalink ?>" class="arrow_link">Read More</a>
                    <? endif ?>
                </div>

            </div>


        </div>
    </div>
</div>
