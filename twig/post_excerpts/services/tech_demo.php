<div class="cell">
    <div class="card primary">

        <div class="card-divider">
            <a href="<?= $post->permalink ?>">
                <h5 class="no-margin">
                    <?= $post->post_title ?>
                </h5>
            </a>

        </div>


        <div class="card-section">

            <?= substr($post->get("content"), 0, 250) ?>&hellip;
            <a href="<?= $post->permalink ?>">Learn More About Our <?= $post->post_title ?></a>

        </div>
    </div>
</div>
