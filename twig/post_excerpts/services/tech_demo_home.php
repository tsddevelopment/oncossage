<div class="cell">
    <div class="card primary">

        <div class="card-section">
            <h3 class="no-margin text-center">
                <?= $post->post_title ?>
            </h3>
            <a href="<?= $post->permalink ?>">
            </a>

            <?= apply_filters("the_content",substr($post->get("content"), 0, 250)) ?>


            <a href="<?= $post->permalink ?>">Read More</a>

        </div>
    </div>
</div>
