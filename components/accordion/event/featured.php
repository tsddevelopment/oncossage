<div class="grid-x grid-padding-x grid-padding-y secondary_xxlight-background">
    <div class="cell auto">
        <h4><?= $post->get("title") ?></h4>
    </div>
    <div class="cell shrink">
        <p><?= $post->get_start_date()->format("l F jS") ?></p>
    </div>
</div>
