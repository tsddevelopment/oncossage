<div class="grid-x grid-padding-x">
    <div class="cell auto">
        <h4><?= $post->get("title") ?></h4>
    </div>
    <div class="cell shrink">
        <p><a href="<?= $post->permalink ?>">Learn More</a></p>
    </div>
</div>
