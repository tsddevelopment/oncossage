<?
$header    = get_field( "primary_header_menu", "option" );
$logo      = get_field( "header_logo", "option" );
$menu_html = wp_nav_menu( [ 'menu'       => $header,
                            'menu_class' => 'vertical menu accordion-menu',
                            'items_wrap' => '<ul id="%1$s" class="%2$s" data-accordion-menu data-submenu-toggle="true">%3$s</ul>',
                            'echo'       => false ] );
?>

<div class="full-height grid-y align-middle padding-box onc-offcanvas">

    <div class="cell shrink header full-width">
        <div class="grid-x align-middle grid-padding-x">
            <div class="cell auto logo">
                <a href="<?= site_url() ?>">
                    <img src="<?= $logo['sizes']['medium'] ?>" alt="">
                </a>
            </div>
            <div class="cell shrink cursor-pointer" data-close>
                <i class="fa fa-times fa-2x"></i>
            </div>
        </div>

    </div>

    <div class="cell shrink my-auto full-width">
        <?= $menu_html ?>
    </div>
</div>