<div class="grid-container fluid no-padding is-standard-desktop-header" is-editor-style>
    <div class="grid-y">
        <div class="cell">
            <div class="spacer xsmall"></div>
        </div>
        <div class="cell">
            <div class="grid-container align-middle">
                <div class="grid-x medium-header align-middle">

                    <? if ( $img ): ?>
                        <div class="cell auto medium-12 xlarge-shrink logo">
                            <a href="<?= site_url() ?>">
                                <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo" />
                            </a>
                        </div>
                    <? endif ?>


                    <div class="cell small-12 xlarge-auto menus show-for-medium">
                        <div class="grid-y">
                            <div class="cell primary-menu-area">
                                <div class="grid-x">
                                    <div class="cell auto">
                                        <? wp_nav_menu( [ 'menu'       => $primary_menu,
                                                          'menu_class' => 'dropdown menu',
                                                          'items_wrap' => '<ul id="%1$s" class="%2$s align-right" data-dropdown-menu>%3$s</ul>',
                                                          'walker'     => new TSD_Infinisite\top_menu_bar_walker() ] ); ?>
                                    </div>
                                    <div class="cell shrink space-left">
                                        <?= onc_get_login_link() ?>
                                    </div>
                                </div>

                            </div>


                            <? if ( $secondary_menu ): ?>

                                <div class="cell small-shrink medium-shrink">

                                    <?
                                    $walker = $secondary_menu_branch_toggle ? new TSD_Infinisite\Display_Parent_Branch() : false;

                                    wp_nav_menu( [ 'menu'       => $secondary_menu,
                                                   'menu_class' => 'dropdown menu',
                                                   'items_wrap' => '<ul id="%1$s" class="%2$s align-right" data-dropdown-menu>%3$s</ul>',
                                                   'walker'     => $walker ] ) ?>

                                </div>

                            <? endif ?>

                            <? if ( $social_media ): ?>
                                <div class="cell small-shrink medium-shrink social-media">
                                    <?= $social_media ?>
                                </div>
                            <? endif ?>
                        </div>
                    </div>


                    <? if ( $overlay_menu ): ?>


                        <div class="cell small-shrink medium-shrink no-padding overlay-menu-launcher">
                            <span class="fa-2x cursor_pointer no_outline"
                                  data-open="overlay-menu-modal"
                                  aria-controls="overlay-menu-modal" aria-haspopup="true" tabindex="0">
                        <?= $overlay_menu_launcher_icon ?>
                            </span>
                        </div>


                        <? include( $_SERVER['DOCUMENT_ROOT'] . $overlay_menu_template ); ?>


                    <? endif ?>


                    <? if ( $offcanvas_menu ): ?>

                        <div class="cell shrink no-padding offcanvas-menu-launcher secondary_light-text hide-for-medium">
                            <span class="fa-2x cursor_pointer no_outline"
                                  data-toggle="offCanvas"
                            >
                                <?= $offcanvas_menu_launcher_icon ?>
                            </span>
                        </div>

                    <? endif ?>

                </div>

            </div>
        </div>
    </div>
</div>

