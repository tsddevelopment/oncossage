<div class="grid-container fluid no-padding">
    <div class="row">
        <div class="spacer xsmall"></div>
        <div class="spacer xsmall"></div>
    </div>
    <div class="grid-container">
        <footer class="grid-x grid-padding-x align-top">
            <? if ( $img ): ?>
                <div class="cell shrink logo">
                    <a href="<?= site_url() ?>">
                        <img src="<?= $img['sizes']['medium'] ?>" alt="Header Logo" />
                    </a>
                </div>
            <? endif ?>

            <!--<div class="show-for-medium medium-1 cell">

            </div>-->

            <div class="cell small-12 medium-7 large-auto">
                <div class="grid-x">
                    <? if ( $primary_content ): ?>
                        <div class="cell auto">
                            <div class="secondary-content">
                                <div class="grid-x align-bottom">
                                    <div class="small-12 xlarge-auto">
                                        <?= $primary_content ?>
                                    </div>
                                </div>
                                <div class="hide-for-medium-only">
                                    <?= \TSD_Infinisite\Acme::get_file( "components/modules/footer-sm.php" ) ?>
                                </div>
                            </div>
                        </div>
                    <? endif ?>

                </div>
            </div>


            <div class="cell small-12 medium-5 large-shrink">

                <? if ( $secondary_content ): ?>
                    <div class="cell auto">
                        <div class="secondary-content">
                            <?= $secondary_content ?>
                        </div>
                    </div>
                <? endif ?>
                <? if ( $primary_menu ): ?>
                    <div class="primary-menu-area">
                        <? wp_nav_menu( [ 'menu'       => $primary_menu,
                                          'menu_class' => 'dropdown menu white-text-children align-right',
                                          'items_wrap' => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>', ] ) ?>
                    </div>
                <? endif ?>
                <? if ( $secondary_menu ): ?>
                    <div class="primary-menu-area">
                        <? wp_nav_menu( [ 'menu'       => $secondary_menu,
                                          'menu_class' => 'dropdown menu white-text-children align-right',
                                          'items_wrap' => '<ul id="%1$s" class="%2$s" data-dropdown-menu>%3$s</ul>', ] ) ?>
                    </div>
                <? endif ?>
                <? if ( $cta_menu ): ?>
                    <div class="primary-menu-area">
                        <? wp_nav_menu( [ 'menu'       => $cta_menu,
                                          'menu_class' => '',
                                          'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', ] ) ?>
                    </div>
                <? endif ?>
            </div>


            <div class="show-for-medium-only full-width medium-elements">
                <?= \TSD_Infinisite\Acme::get_file( "components/modules/footer-sm.php" ) ?>
            </div>


        </footer>
    </div>
    <div class="row">
        <div class="spacer small"></div>
    </div>
    <div class="dark-gray-bg">
        <div class="grid-container">
            <div class="grid-x grid-padding-x grid-padding-y align-top text-white">
                <div class="cell small-12">
                    <span class="copyright">&copy; <?php echo date( 'Y' ) . ' ' . get_bloginfo( 'name' ) . '. All Rights Reserved'; ?></span>
                </div>
            </div>
        </div>
    </div>
</div>