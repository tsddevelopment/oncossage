<? $post = new \TSD_Infinisite\IS_Post($id); ?>
<?= divider('right') ?>
<h4 class="text-center">Ready to Learn More?</h4>
<?= do_shortcode("[gravityform id='2' title='false' description='false']") ?>


<?= divider('right', 'small', 'secondary') ?>
<h6 class="text-right"><a href="<?= $post->permalink ?>" class="secondary-text">
        <i class="far fa-long-arrow-left space-right"></i>
        Back to <?= $post->post_title ?>
    </a></h6>