<? if (get_the_ID() == 89) return ?>

<div class="grid-container onc-coming-soon">
    <div class="p-3 my-4 mx-auto">
        <h2 class="title">Welcome!</h2>
        <p class="text">We're testing out a few things before our official launch. Check back soon for booking
                        information!</p>
    </div>
</div>