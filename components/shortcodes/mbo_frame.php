<?php

$type = $vars['atts'][0];


$embed_codes = ['prospect'     => '<healcode-widget data-type="prospects" 
                                data-widget-partner="object" 
                                data-widget-id="6c32013d986" 
                                data-widget-version="0" ></healcode-widget>',
                'staff'        => '<healcode-widget data-type="staff_lists" 
                                data-widget-partner="object" 
                                data-widget-id="6c26643d986" 
                                data-widget-version="0" ></healcode-widget>',
                'enrollments'  => '<healcode-widget 
                                data-type="enrollments" 
                                data-widget-partner="object" 
                                data-widget-id="6c57729d986" 
                                data-widget-version="0" ></healcode-widget>',
                'appointment'  => '<healcode-widget 
                                data-type="appointments" 
                                data-widget-partner="object" 
                                data-widget-id="6c62326d986" 
                                data-widget-version="0" ></healcode-widget>',
                'registration' => '<healcode-widget data-type="registrations" 
                                data-widget-partner="object" 
                                data-widget-id="6c88100d986" 
                                data-widget-version="0" ></healcode-widget>'];

if (!array_key_exists($type, $embed_codes)):
    print "Key $type not found";
    return false;
endif;

print $embed_codes[$type];