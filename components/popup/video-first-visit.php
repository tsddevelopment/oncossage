<div id="videoModalFirstVisit">
    <div class="popup-container">
        <h2 class="">Welcome to Oncossage</h2>
        <p class="">To learn more about us, please click on the video below.</p>
        <div class="spacer"></div>
        <video id="oncossage-video-intro"
               poster="/wp-content/uploads/2019/11/oncossage-video-poster.png"
               autoplay
        >
            <source src="http://oncossage.wpengine.com/wp-content/uploads/2019/11/AUDIO_ONCOSSAGE_BO.mp4"
                    type="video/mp4" />
        </video>
        <div class="spacer large"></div>
        <a href="#" class="button large" id="close_splash">Continue to Oncossage</a>
    </div>
</div>