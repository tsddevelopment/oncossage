function get_size() {

    let sizes = {
        'small': 700,
        'medium': 1024,
        'large': 1200,
        'xlarge': 1440,
        'xxlarge': 160,
    };

    sizes.each(function () {

    });

}

function init_testimonial_slider() {

    let slider = $(".apc-testimonial-slider");

    slider.owlCarousel({
        items: 1,
        autoHeight: true
    });

}

function init_popup_video_on_first_visit() {

    if (!$("body").hasClass("home"))
        return;


    function init_splash_vid() {
        let splash_vid = $("#oncossage-video-intro");
        set_toggle(splash_vid);

        splash_vid
            .on('ended', function () {
                close_splash();
            })
    }

    $(window).load(init_splash_vid());

    let splash_vid = $("#oncossage-video-intro");
    let hero_vid = $("#home-hero-video");
    set_toggle(hero_vid);

    function set_toggle(video) {
        video.on('click', function (e) {
            let vid = e.target;
            if (!vid.paused) {
                vid.pause();
                return false;
            }
            vid.play();
        })
    }

    function close_splash() {
        $("#videoModalFirstVisit").fadeOut();
        $("#oncossage-video-intro").get(0).pause();

        setTimeout(function () {
            $("#home-hero-video").trigger('click');
        }, 750);
    }

    $("#close_splash").click(function () {
        close_splash()
    });


}

function hack_card_eq() {


    let row = $(".equalize-cards");

    if ($(window).width() <= 700) {
        row.find(".vc_column-inner > .wpb_wrapper").height("auto");
        return true;
    }


    row.each(function () {
        let r = $(this),
            maxHeight = 0,
            cards = r.find(".onc-card-column");
        cards.find(".vc_column-inner > .wpb_wrapper").height("auto");

        cards.each(function () {
            let c = $(this);
            maxHeight = c.height() > maxHeight ? c.height() : maxHeight;
        });

        cards.find(".vc_column-inner > .wpb_wrapper").height(maxHeight + "px");

    })


}

function hack_spacing() {

    let rows = $(".onc-interior-hero");

    rows.each(function () {
        let r = $(this);

        r.parents(".vc_column-inner").css("padding-top", "0");
    })

}

function setCookie(name, value, days) {
    let expires = "";
    if (days) {
        let date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + expires + "; path=/";
}

function getCookie(name) {
    let nameEQ = name + "=";
    let ca = document.cookie.split(';');
    for (let i = 0; i < ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-99999999;';
}

function init_pricing_table() {

    let table = jQuery(".pricing_table");

    if (!table.length) return;

    let cell = table.find("td"),
        c = 0;

    cell.each(function () {
        let cell = jQuery(this),
            col = cell.hasClass("onc"),
            yes = cell.text().indexOf('Yes') !== -1;

        if (yes && col) {
            cell.addClass("success_xdark-background white-text");
            c++;
        }


    })

}

function hack_plwc_title() {

    let title = jQuery("#plwc-header");

    if (!title.length) return;




}


hack_spacing();
init_testimonial_slider();
init_popup_video_on_first_visit();
hack_card_eq();
init_pricing_table();
hack_plwc_title();


$(document).foundation();

$(function () {
    // $('.onc-card-column > .vc_column-inner > .wpb_wrapper').matchHeight();
    // $('.onc-card-column > .vc_column-inner > .wpb_wrapper > h3').matchHeight();
    // $('.onc-card-column > .vc_column-inner > .wpb_wrapper > .wpb_text_column').matchHeight();
});

$(window).on('resize', function () {
    hack_card_eq();
});