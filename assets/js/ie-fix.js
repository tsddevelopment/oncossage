function msieversion() {

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE ");

    // console.log(ua);
    if (msie > 0)
    {
        return parseInt(ua.substring(msie + 5, ua.indexOf(".", msie)));
    }
    else if(ua.match(/Trident.*rv\:11\./)){
        return 11;
    }  // If Internet Explorer, return version number)
    else if(ua.match(/Edge\//)){
        return parseInt(ua.substring(ua.indexOf("Edge/") + 5));
    }
    else  // If another browser, return 0
    {
        return -1;
    }

}
var IEversion = msieversion();
if(IEversion){
    var doc = document.documentElement;
    doc.setAttribute('data-useragent', navigator.userAgent);

    if(IEversion >= 10){
        var $heroContainer = jQuery('#hero_container');
        console.log($heroContainer.width());
        $heroContainer.css({height:$heroContainer.width() * 0.0455 + "px"});

        $( window ).resize(function() {
            console.log($heroContainer.width());
            $heroContainer.css({height:$heroContainer.width() * 0.0455 + "px"});
        });
    }
}
